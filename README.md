# Rockstar Coders Test

As instructed, create-react-app was used. Thus the app is a full SPA. No SSR or other cool things. 
Since the FAQ says "we only care about the code" the UI is *really* basic. I used ChakraUI because I've wanted to try it for a while now and to not really think about the visuals that much.

## To run it:

In the project directory, you can run:

`npm install`

`npm start`

NOTE: you need to have an environment variable with your api key called `REACT_APP_MOVIE_API_KEY`. Since the app is fully client-side, yes you can get it from the inspector.

## To view the app

The app is deployed to netlify [here](https://rockstarcoderstest.netlify.app).

