import { useState, useEffect } from "react"
import axios from "axios"

const useDiscover = () => {
  const [movies, setMovies] = useState( null )
  const [search, setSearch] = useState( null )
  const [isLoading, setIsLoading] = useState( true )
  const API_KEY = process.env.REACT_APP_MOVIE_API_KEY
  const BASE_URL = "https://api.themoviedb.org/3/"

  const setMoviesFromResults = ( { data } ) => {
    setMovies( data.results )
    setIsLoading( false )
  }

  useEffect( () => {
    if ( search ) {
      setIsLoading( true )
      const query = encodeURIComponent( search )

      axios
        .get(
          `${BASE_URL}search/movie?api_key=${API_KEY}&query=${query}&include_adult=false`
        )
        .then( setMoviesFromResults )
    } else {
      
      setIsLoading( true )
      axios
        .get( `${BASE_URL}discover/movie?api_key=${API_KEY}` )
        .then( setMoviesFromResults )
    }
  }, [search] )

  return [movies, setSearch, isLoading]
}

export default useDiscover
