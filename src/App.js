import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'
import { ThemeProvider, theme, Box } from "@chakra-ui/core"
import NavBar from './components/NavBar'
import Discover from './pages/Discover'
import './App.css'
import { ApiConfigProvider } from './context/apiConfig'
import { CurrentMovieProvider } from './context/currentMovie'
import Details from './pages/Details'

const Content = ( {children} ) => (
  <Box className='Content' maxWidth='1200px' margin='auto' display='flex' justifyContent='center'>
    {children}
  </Box>
)


const App = ( ) => {

  return (
    <ThemeProvider theme={theme}>
      <Box width='100%' className='App'>
        <NavBar />
        <ApiConfigProvider>
          <CurrentMovieProvider>
            <Content>
              <Switch>
                <Route exact path='/' component={Discover} />
                <Route path='/details' component={Details} />
              </Switch>
            </Content>
          </CurrentMovieProvider>
        </ApiConfigProvider>
      </Box>
    </ThemeProvider>
  )
}

const RoutedApp = () =>  ( 
  <Router>
    <App />
  </Router> 
)



export default RoutedApp
