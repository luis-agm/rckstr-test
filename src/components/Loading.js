import React from 'react'
import {  Box, Spinner } from '@chakra-ui/core'

const LoadingBox = () => (
  <Box width='100%' p='10'>
    <Spinner />
  </Box>
)

export default LoadingBox