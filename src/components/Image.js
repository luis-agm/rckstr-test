import React, { useContext } from "react"
import { Image as ChakraImg } from "@chakra-ui/core"
import ApiConfigContext from "../context/apiConfig"

const Image = ( { path, type, fileSize, ...restProps } ) => {
  // Yes, this should be lazyloaded using intersection-observer and stuff, but come on...

  const { config } = useContext( ApiConfigContext )
  const { images } = config
  const baseUrl = images.secure_base_url

  const availableSizes = images[`${type}_sizes`]
  const actualSize =
    availableSizes.indexOf( fileSize ) >= 0
      ? fileSize
      : images[`${type}_sizes`][0]
  const actualUrl = `${baseUrl}${actualSize}${path}`

  return <ChakraImg src={actualUrl} {...restProps} />
}

export default Image
