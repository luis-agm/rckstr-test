import React from 'react'
import { Box, Heading } from '@chakra-ui/core'
import Image from './Image'

const MovieResultCard = ( {movie, clickHandler} ) => {
  const {original_title, poster_path} = movie

  return (
    <Box onClick={clickHandler} maxW='185px' backgroundColor='#282a36' color='#f8f8f2' rounded='lg' overflow='hidden'>
      <Image path={poster_path} type='poster' fileSize='w185' /> 
      <Heading size='sm' p='5px'>{original_title}</Heading>
    </Box>
  )
}

export default MovieResultCard
