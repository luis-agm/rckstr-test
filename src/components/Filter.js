import React from "react"
import { Box } from "@chakra-ui/core"
import Star from "./Star"

const Filter = ( { currentRating, setRating } ) => {
  const ratings = ["0-2", "2-4", "4-6", "6-8", "8-10"]

  return (
    <Box p='5px' d='flex' justifyContent='space-between' width='170px'>
      {ratings.map( ( rating, index ) => {
        const isFilled = index <= ratings.indexOf( currentRating )
        
        return (
          <Box
            key={rating}
            onClick={() => {
              setRating( rating === currentRating ? null : rating )
            }}
          >
            <Star filled={isFilled} />
          </Box>
        )
      } )}
    </Box>
  )
}

export default Filter
