import React from 'react'
import { Heading } from '@chakra-ui/core'
import styled from '@emotion/styled'
import { Link } from 'react-router-dom'

const Nav = styled.div`
    background: #0F0F0F;
    display: flex;
    flex-flow: row nowrap;
    justify-content: flex-start;
    padding: 0 40px;
    color: white;
`

export default function Navbar() {
  return (
    <Nav>
      <Link to='/'>
        <Heading>
          MovieStuff 
        </Heading>
      </Link>
    </Nav>
  )
}
