import React, { useState, useEffect, useRef } from "react"
import { Input, InputGroup, InputRightAddon, Box } from "@chakra-ui/core"
import Filter from "./Filter"
import { debounce } from "../utils"

const SearchBar = ( { setSearch, currentRating, setCurrentRating } ) => {
  const [searchQuery, setSearchQuery] = useState( "" )
  const setSearchTerm = useRef( debounce( ( value ) => setSearch( value ), 400 ) )

  const onChangeSearch = ( searchTerm ) => {
    setSearchQuery( searchTerm )
  }

  useEffect( () => {
    if ( searchQuery ) {
      setSearchTerm.current( searchQuery )
    }
  }, [searchQuery] )

  return (
    <Box minWidth='100%'>
      <InputGroup>
        <Input
          value={searchQuery}
          onChange={( evt ) => onChangeSearch( evt.target.value )}
          placeholder='Search for a movie...'
        />
        <InputRightAddon bg='#282a36'>
          <Filter currentRating={currentRating} setRating={setCurrentRating} />
        </InputRightAddon>
      </InputGroup>
    </Box>
  )
}

export default SearchBar
