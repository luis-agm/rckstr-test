import React, {useContext} from 'react'
import CurrentMovieContext from '../context/currentMovie'
import { Box, Heading, Text, Button } from '@chakra-ui/core'
import Image from '../components/Image'
import { Link } from 'react-router-dom'

const Details = (  ) => {
  const { currentMovie } = useContext( CurrentMovieContext )

  return ( currentMovie ?
    <Box>
      <Link to='/'>
        <Button m='10'>Go back home</Button>
      </Link>

      <Box d='flex' flexDirection='column' width='28rem' minHeight='28rem' color='#f8f8f2' rounded='lg' overflow='hidden'>
        <Image type='backdrop' width='100%' fileSize='w500' path={currentMovie.backdrop_path} />
        <Box p='15px' flex='1' backgroundColor='#282a36' textAlign='left'>
          <Heading size='sm'>{currentMovie.original_title}</Heading>
          <Text p='10px'>{currentMovie.overview}</Text>
          <Text>Rating: {currentMovie.vote_average}</Text>
        </Box>
      </Box>
    </Box> :
    null
  )
}

export default Details
