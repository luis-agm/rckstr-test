import React, { useEffect, useContext, useState } from 'react'
import { SimpleGrid, Box } from '@chakra-ui/core'
import SearchBar from '../components/SearchBar'
import useDiscover from '../hooks/useDiscover'
import MovieResultCard from '../components/MovieCard'
import LoadingBox from '../components/Loading'
import CurrentMovieContext from '../context/currentMovie'

const Results = ( {movies, selectMovie} ) => {
  return (
    <SimpleGrid p={15} columns={[2,null,3,4,5]} gap={6}>
      {movies.map( 
        ( movie ) => <MovieResultCard key={movie.id} movie={movie} clickHandler={() => selectMovie( movie.id )} />
      )}
    </SimpleGrid>
  )
}

const Discover = ( { history } ) => {
  const [movies, setSearch, isLoading] = useDiscover()
  const [filteredMovies, setFilteredMovies] = useState( movies )
  const [currentRating, setCurrentRating] = useState( undefined )
  const { setCurrentMovie } = useContext( CurrentMovieContext )

  useEffect( () => {
    if( currentRating ) {
      const [min,max] = currentRating.split( '-' )
      const filtered = movies.filter( ( mv ) => mv.vote_average >= min && mv.vote_average <= max )

      setFilteredMovies( filtered )
    } else {
      setFilteredMovies( movies )
    }
  }, [currentRating, isLoading] )

  const selectMovieById = ( id ) => {
    const movie = movies.find( ( mv ) => mv.id === id )
    setCurrentMovie( movie )
    history.push( '/details' )
  }

  return ( 
    <Box p='40px'>
      <SearchBar setSearch={setSearch} currentRating={currentRating} setCurrentRating={setCurrentRating} />
      {isLoading && <LoadingBox  />}
      {filteredMovies && !isLoading && (
        <Results movies={filteredMovies} selectMovie={selectMovieById} />
      )}
    </Box>
  )
}


export default Discover