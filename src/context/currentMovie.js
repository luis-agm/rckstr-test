import React, { useState } from 'react'

const CurrentMovieContext = React.createContext( null )

const { Provider } = CurrentMovieContext

export const CurrentMovieProvider = ( { children } ) => {
  const [currentMovie, setCurrentMovie] = useState( null )

  const clearCurrentMovie = () => setCurrentMovie( null )
 
  const state = {
    currentMovie,
    setCurrentMovie,
    clearCurrentMovie
  }

  return (
    <Provider value={state}>
      {children}
    </Provider>
  )
}

export default CurrentMovieContext