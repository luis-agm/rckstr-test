import React, {useState, useEffect} from 'react'
import axios from 'axios'
import LoadingBox from '../components/Loading'

const ApiConfigContext = React.createContext( null )

const {Provider} = ApiConfigContext

export const ApiConfigProvider = ( {children} ) => {
  const [isLoading, setIsLoading] = useState( true )
  const [configResult, setConfigResult] = useState( null )
  const API_KEY = process.env.REACT_APP_MOVIE_API_KEY
  const BASE_URL = "https://api.themoviedb.org/3/"

  const getApiConfig = async () => {
    axios.get( `${BASE_URL}configuration?api_key=${API_KEY}` ).then( ( {data} )=>{
      setConfigResult( data )
      setIsLoading( false )
    } )
  }
 
  useEffect( ()=>{
    getApiConfig()
  }, [] )

  return (

    <Provider value={{isLoading, config: configResult}}>
      {isLoading ? <LoadingBox /> :  children}
    </Provider>
  )
}

export default ApiConfigContext
